﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Moq;
using RestSharp;
using System.Web.Script.Serialization;

using _18F_BPA_Alpha_Synergetics.Controllers;
using Synergetics.Service.Interface;
using Synergetics.OpenFDA.Model;
using Microsoft.AspNet.Mvc;
using System.Diagnostics;
using System.Collections;

namespace Synergetics.Tests
{
    public class HomeControllerTests : Tests    {
        HomeController _controller;
        public HomeControllerTests()
        {
            var foodMock = new Mock<IFoodsService>();
            var drugsMock = new Mock<IDrugsService>();
            var deviceMock = new Mock<IDevicesService>();

            var responseMock = new Mock<IRestResponse>();


            List<StateCount> stateCounts = new List<StateCount>
            {
                new StateCount {term = "nm", count = 127 },
                new StateCount {term = "il", count = 145 },
                new StateCount {term = "mn", count = 87 },
                new StateCount {term = "co", count = 20 },
                new StateCount {term = "wi", count = 177 },
                new StateCount {term = "in", count = 152 },
                new StateCount {term = "ca", count = 65 },
                new StateCount {term = "nv", count = 42 },
                new StateCount {term = "fl", count = 95 },
                new StateCount {term = "ny", count = 105 }
            };

            var response = new Results { results = stateCounts };


            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string json = serializer.Serialize(response);

            //new up Query parameters
            var term = "salmonella";
            var crit = new QueryParameters
            {
                search = $"reason_for_recall:\"{term}\"",
                count = "state"
            };

            responseMock.Setup(x => x.Content).Returns(json);
            var task = Task.FromResult(responseMock.Object);

            foodMock.Setup(x => x.GetEnforcementReports(It.IsAny<QueryParameters>())).Returns(task);
            drugsMock.Setup(x => x.GetEnforcementReports(It.IsAny<QueryParameters>())).Returns(task);
            deviceMock.Setup(x => x.GetEnforcementReports(It.IsAny<QueryParameters>())).Returns(task);
            string events = task.Result.Content;

            _controller = new HomeController(drugsMock.Object, deviceMock.Object, foodMock.Object);
        }

        public void TestGetFillKey()
        {
            Assert(_controller.GetFillKey(-23) == "none", "negative");
            Assert(_controller.GetFillKey(0) == "none", "no fill");
            Assert(_controller.GetFillKey(4) == "lowest", "lowest fill");
            Assert(_controller.GetFillKey(50) == "low", "low fill");
            Assert(_controller.GetFillKey(140) == "medium", "medium fill");
            Assert(_controller.GetFillKey(522) == "high", "high fill");
            Assert(_controller.GetFillKey(2323232) == "highest", "highest fill");
        }

        public void TestReports()
        {

            var reportsTest = (JsonResult)_controller.Reports(new _18F_BPA_Alpha_Synergetics.ViewModel.MapCriteriaViewModel { Id = "salmonella", Devices = true, Drugs = true, Foods = true });

            var list = ((IList)reportsTest.Value);

            Assert(reportsTest.Value is IList && ((IList)reportsTest.Value).Count == 10);

            // Assert(reportsTest.ExecuteResultAsync().GetAwaiter().GetResult();
        }
    }
}
