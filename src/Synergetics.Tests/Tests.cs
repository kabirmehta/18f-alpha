﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Synergetics.Tests
{
    public class Tests
    {
        protected void Assert(bool testCase, string description = "All")
        {
            var method = new StackTrace().GetFrame(1).GetMethod();
            
            string status = testCase ? "Passed" : "Failed";

            Console.WriteLine($"{method.DeclaringType.Name} | {method.Name} | {description} | {status}");
        }
    }
}
  