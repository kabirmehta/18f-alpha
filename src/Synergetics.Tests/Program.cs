﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Moq;
using RestSharp;
using System.Web.Script.Serialization;

using _18F_BPA_Alpha_Synergetics.Controllers;
using Synergetics.Service.Interface;
using Synergetics.OpenFDA.Model;
using Microsoft.AspNet.Mvc;
using System.Diagnostics;
using System.Collections;

namespace Synergetics.Tests
{
    public class Program
    {
        public void Main(string[] args)
        {
            var hcTests = new HomeControllerTests();
            hcTests.TestReports();
            hcTests.TestGetFillKey();

            Console.ReadKey();
        }
        
    }

    public class StateCount
    {
        public string term { get; set; }
        public int count { get; set; }
    }

    public class Results
    {
        public IList<StateCount> results { get; set; }
    }
}
