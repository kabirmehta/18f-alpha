﻿using RestSharp;
using Synergetics.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Synergetics.OpenFDA
{
    public class OpenFDAService
    {
        static string apiKey = "srgEEoJS5mPZiZ0qUphigb7uIikI3Bpvusmt6V23";
        static string root = "http://api.fda.gov";


        public class DrugsService : IDrugsService
        {
            private readonly RestClient _client;
            public DrugsService()
            {
                _client = new RestClient($"{root}/drug");
                _client.AddDefaultUrlSegment("api_key", apiKey);
            }

            public async Task<IRestResponse> GetEvents(IQueryParameters parameters)
            {
                var request = new RestRequest("event.json", Method.GET);
                if (parameters != null)
                {
                    request.AddObject(parameters);
                }
                return await _client.ExecuteGetTaskAsync(request);

            }
            public async Task<IRestResponse> GetEnforcementReports(IQueryParameters parameters)
            {
                var request = new RestRequest("enforcement.json", Method.GET);
                if (parameters != null)
                {
                    request.AddObject(parameters);
                }
                return await _client.ExecuteGetTaskAsync(request);

            }
            public async Task<IRestResponse> GetLabels(IQueryParameters parameters)
            {
                var request = new RestRequest("label.json", Method.GET);
                if (parameters != null)
                {
                    request.AddObject(parameters);
                }
                return await _client.ExecuteGetTaskAsync(request);

            }
        }

        public class DevicesService : IDevicesService
        {
            private readonly RestClient _client;

            public DevicesService()
            {
                _client = new RestClient($"{root}/device");
                _client.AddDefaultUrlSegment("api_key", apiKey);

            }
            public async Task<IRestResponse> GetEvents(IQueryParameters parameters)
            {
                var request = new RestRequest("event.json", Method.GET);
                if (parameters != null)
                {
                    request.AddObject(parameters);
                }
                return await _client.ExecuteGetTaskAsync(request);

            }
            public async Task<IRestResponse> GetEnforcementReports(IQueryParameters parameters)
            {
                var request = new RestRequest("enforcement.json", Method.GET);
                if (parameters != null)
                {
                    request.AddObject(parameters);
                }
                return await _client.ExecuteGetTaskAsync(request);

            }
        }

        public class FoodsService : IFoodsService
        {
            private readonly RestClient _client;

            public FoodsService()
            {
                _client = new RestClient($"{root}/food");
                _client.AddDefaultUrlSegment("api_key", apiKey);
            }

            public async Task<IRestResponse> GetEnforcementReports(IQueryParameters parameters)
            {
                var request = new RestRequest("enforcement.json", Method.GET);
                if (parameters != null)
                {
                    request.AddObject(parameters);
                }
                return await _client.ExecuteGetTaskAsync(request);
                
            }
        }
    }
}
