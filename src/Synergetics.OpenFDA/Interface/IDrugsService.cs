﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Synergetics.Service.Interface
{
    public interface IDrugsService
    {
        Task<IRestResponse> GetEvents(IQueryParameters parameters);

        Task<IRestResponse> GetEnforcementReports(IQueryParameters parameters);

        Task<IRestResponse> GetLabels(IQueryParameters parameters);

    }
}
