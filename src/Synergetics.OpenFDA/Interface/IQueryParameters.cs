﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Synergetics.Service.Interface
{
    public interface IQueryParameters
    {
        string search { get; }
        string count { get;}
        int? limit { get; }
        int? skip { get; }
    }
}
