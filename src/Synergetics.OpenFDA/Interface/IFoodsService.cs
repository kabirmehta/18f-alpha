﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Synergetics.Service.Interface
{


    public interface IFoodsService
    {
        Task<IRestResponse> GetEnforcementReports(IQueryParameters parameters);
    }
}
