﻿using System;
using Synergetics.Service.Interface;

namespace Synergetics.OpenFDA.Model
{
    public class QueryParameters : IQueryParameters
    {
        public string search { get; set; }
        public int? skip { get; set; }
        public int? limit { get; set; }
        public string count { get; set; }
    }
} 