﻿using RestSharp.Deserializers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Synergetics.OpenFDA.Model
{
    //objects used for deserialization
    public class Meta
    {
        public string Disclaimer { get; set; }
        public string LastUpdated{ get; set; }
        public string License { get; set; }
    }
    public class Result
    {
        public string Term { get; set; }
        public string Count { get; set; }
    } 

    public class OpenFDACount
    {
        public Meta Meta { get; set; }
        public List<Result> Results { get; set; }
    }
}
