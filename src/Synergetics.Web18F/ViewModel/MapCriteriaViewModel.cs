﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace _18F_BPA_Alpha_Synergetics.ViewModel
{
    public class MapCriteriaViewModel
    {
        public string Id { get; set; }
        public bool Foods { get; set; }
        public bool Drugs{ get; set; }
        public bool Devices{ get; set; }
    }
}
