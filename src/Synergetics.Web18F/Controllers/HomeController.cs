﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Synergetics.Service.Interface;
using Synergetics.OpenFDA.Model;
using Newtonsoft.Json.Linq;
using _18F_BPA_Alpha_Synergetics.ViewModel;

namespace _18F_BPA_Alpha_Synergetics.Controllers
{
    public class HomeController : Controller
    {
        private readonly IDrugsService _drugService;
        private readonly IFoodsService _foodService;
        private readonly IDevicesService _devicesService;

        public HomeController(IDrugsService drugsService, IDevicesService devicesService, IFoodsService foodsService)
        {
            _drugService = drugsService;
            _foodService = foodsService;
            _devicesService = devicesService;
        }

        /// <summary>
        /// Directs MVC to load the Views/Home/Index.cshtml View
        /// </summary>
        /// <returns></returns>
        public IActionResult Index()
        {
            return View();
        }

  
        /// <summary>
        /// Enforement Reports controller to create the shape of the data that is passed to the client side
        /// </summary>
        /// <param name="viewModel">User Interaction Information</param>
        /// <returns></returns>
        public IActionResult Reports(MapCriteriaViewModel viewModel)
        {
            //new up search parameters
            var crit = new QueryParameters
            {
                search = $"reason_for_recall:\"{viewModel.Id}\"",
                count = "state"
            };

            //create async lazy-loading tasks for later execution
            var foodReportsTask = _foodService.GetEnforcementReports(crit);
            var drugReportsTask = _drugService.GetEnforcementReports(crit);
            var devicesReportsTask = _devicesService.GetEnforcementReports(crit);

            //new up async task execution array
            var tasks = new List<Task<RestSharp.IRestResponse>>();

            //add to async task execution array only if the user has selected the corresponding api noun
            if (viewModel.Foods)
            {
                tasks.Add(foodReportsTask);
            }

            if (viewModel.Drugs)
            {
                tasks.Add(drugReportsTask);
            }

            if (viewModel.Devices)
            {
                tasks.Add(devicesReportsTask);
            }

            //no tasks? job done.
            if (tasks.Count() > 0)
            {
                //execute tasks in execution array concurrently. web requests do not wait for the previous one to complete.
                var apiCalls = Task.WhenAll(tasks.ToArray());

                //the results will return as separate json responses
                //only process resopnses that had matches from the api
                //flatten the separate responses into a single json string and capture the results
                //create a collection of tuples with the state (term) and counts
                var separate = apiCalls.Result.Where(m => m.StatusCode != System.Net.HttpStatusCode.NotFound) 
                                            .SelectMany(m => JObject.Parse(m.Content)["results"]?           
                                            .Select(n => Tuple.Create(n["term"]?.ToString().ToUpper(), Convert.ToInt32(n["count"]?.ToString()))));

                //we have a collection of tuples with states and counts
                //the problem is that states are duplicated because we fetched counts from multiple apis
                //we want to combine the counts from all of the apis, so we group by state and sum on reports
                //the result is a list of unique states with the combined counts of enforcement reports from all user selected api nouns (drugs, foods, and/or devices)
                var grouped = from s in separate
                              group s by s.Item1 into g
                              let reports = g.Sum(s => s.Item2)
                              select new
                              {
                                  s = g.Key,
                                  p = new
                                  {
                                      enforcementReports = reports,
                                      fillKey = GetFillKey(reports)
                                  }
                              };

                //return our processed, ready for angular-datamaps json
                return Json(grouped.ToList());
            }
            //no matches. throw an exception to let our client side code know to handle differently (like an error).
            throw new KeyNotFoundException("No matches");
        }

        public string GetFillKey(int count)
        {
            if (count == 0)
            {
                return "none";
            }
            else if (count >= 1 && count < 10)
            {
                return "lowest";
            }
            else if (count >= 10 && count < 100)
            {
                return "low";
            }
            else if (count >= 100 && count < 500)
            {
                return "medium";
            }
            else if (count >= 500 && count < 1000)
            {
                return "high";
            }
            else if (count > 1000)
            {
                return "highest";
            }
            return "none";
        }

        public IActionResult Error()
        {
            return View("~/Views/Shared/Error.cshtml");
        }
    }
}
