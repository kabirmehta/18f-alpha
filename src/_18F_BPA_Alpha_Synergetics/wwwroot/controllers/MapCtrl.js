﻿// Scope as the parent of the angular-datamap directive

var app = angular.module('app', [
        'datamaps'
]);
app.controller('MapCtrl', function ($scope, $http) {
    $scope.plugins = {
        customLegend: function (layer, data, options) {
            var html = ['<ul class="list-inline">'],
                label = '';
            for (var fillKey in this.options.fills) {
                html.push('<li class="key" ',
                            'style="border-top: 10px solid ' + this.options.fills[fillKey] + '">',
                            (fillKey === 'defaultFill' ? 'N/A' : fillKey),
                            '</li>');
            }
            html.push('</ul>');
            d3.select(this.options.element).append('div')
              .attr('class', 'datamaps-legend')
              .style('position', 'absolute')
              .style('bottom', 0)
              .html(html.join(''));
        }
    };
    $scope.search = "";

    $scope.doSearch = function () {

        $http.get('/home/reports/' + $scope.search).
           success(function (data, status, headers, config) {
               if (data === null) {
                   for (var existingItem in $scope.map.data) {
                       $scope.map.data[existingItem].enforcementReports = 0;
                       $scope.map.data[existingItem].fillKey = "none";
                   }
               }
               else {
                   i = 0;
                   while (i < data.length) {
                       var existingItem = $scope.map.data[data[i].s];

                       if (existingItem != null && existingItem !== 'undefined') {
                           existingItem.enforcementReports = data[i].p.enforcementReports;
                           existingItem.fillKey = data[i].p.fillKey;
                       }
                       i++;
                   }
               }
           }).
           error(function (data, status, headers, config) {
           });
    };

    $scope.map = {
        scope: 'usa',
        responsive: true,
        options: {
            staticGeoData: true
        },
        geographyConfig: {
            highlightBorderColor: '#bada55',
            popupTemplate: function (geography, data) {
                return '<div class="hoverinfo">' +
                         geography.properties.name +
                         'Enforcement Reports: ' +
                         (data.enforcementReports || '0') +
                       '</div>';
            },
            highlightBorderWidth: 3
        },
        fills: {
            'none': '#ACF0F2',
            'lowest': '#004358',
            'low': '#1F8A70',
            'medium': '#BEDB39',
            'high': '#FFE11A',
            'highest': '#FD7400',
            defaultFill: '#FD7400'
        },
        data: {
            "AZ": {
                "fillKey": "highest",
                "enforcementReports": 5
            },
            "CO": {
                "fillKey": "lowest",
                "enforcementReports": 5
            },
            "DE": {
                "fillKey": "low",
                "enforcementReports": 32
            },
            "FL": {
                "enforcementReports": 29,
                "fillKey": "defaultFill"
            },
            "GA": {
                "fillKey": "highest",
                "enforcementReports": 32
            },
            "HI": {
                "fillKey": "low",
                "enforcementReports": 32
            },
            "ID": {
                "fillKey": "highest",
                "enforcementReports": 32
            },
            "IL": {
                "fillKey": "low",
                "enforcementReports": 32
            },
            "IN": {
                "fillKey": "highest",
                "enforcementReports": 11
            },
            "IA": {
                "fillKey": "lowest",
                "enforcementReports": 11
            },
            "KS": {
                "fillKey": "highest",
                "enforcementReports": 32
            },
            "KY": {
                "fillKey": "highest",
                "enforcementReports": 32
            },
            "LA": {
                "fillKey": "highest",
                "enforcementReports": 32
            },
            "MD": {
                "fillKey": "medium",
                "enforcementReports": 32
            },
            "ME": {
                "fillKey": "low",
                "enforcementReports": 32
            },
            "MA": {
                "fillKey": "low",
                "enforcementReports": 32
            },
            "MN": {
                "fillKey": "low",
                "enforcementReports": 32
            },
            "MI": {
                "fillKey": "low",
                "enforcementReports": 32
            },
            "MS": {
                "fillKey": "none",
                "enforcementReports": 32
            },
            "MO": {
                "fillKey": "highest",
                "enforcementReports": 13
            },
            "MT": {
                "fillKey": "medium",
                "enforcementReports": 32
            },
            "NC": {
                "fillKey": "high",
                "enforcementReports": 32
            },
            "NE": {
                "fillKey": "highest",
                "enforcementReports": 32
            },
            "NV": {
                "fillKey": "low",
                "enforcementReports": 32
            },
            "NH": {
                "fillKey": "lowest",
                "enforcementReports": 32
            },
            "NJ": {
                "fillKey": "low",
                "enforcementReports": 32
            },
            "NY": {
                "fillKey": "low",
                "enforcementReports": 32
            },
            "ND": {
                "fillKey": "medium",
                "enforcementReports": 32
            },
            "NM": {
                "fillKey": "none",
                "enforcementReports": 32
            },
            "OH": {
                "enforcementReports": 32,
                "fillKey": "defaultFill"
            },
            "OK": {
                "fillKey": "medium",
                "enforcementReports": 32
            },
            "OR": {
                "fillKey": "low",
                "enforcementReports": 32
            },
            "PA": {
                "fillKey": "none",
                "enforcementReports": 32
            },
            "RI": {
                "fillKey": "medium",
                "enforcementReports": 32
            },
            "SC": {
                "fillKey": "medium",
                "enforcementReports": 32
            },
            "SD": {
                "fillKey": "highest",
                "enforcementReports": 32
            },
            "TN": {
                "fillKey": "highest",
                "enforcementReports": 32
            },
            "TX": {
                "fillKey": "highest",
                "enforcementReports": 32
            },
            "UT": {
                "fillKey": "medium",
                "enforcementReports": 32
            },
            "WI": {
                "fillKey": "medium",
                "enforcementReports": 32
            },
            "VA": {
                "fillKey": "none",
                "enforcementReports": 32
            },
            "VT": {
                "fillKey": "low",
                "enforcementReports": 32
            },
            "WA": {
                "fillKey": "low",
                "enforcementReports": 32
            },
            "WV": {
                "fillKey": "highest",
                "enforcementReports": 32
            },
            "WY": {
                "fillKey": "none",
                "enforcementReports": 32
            },
            "CA": {
                "fillKey": "low",
                "enforcementReports": 32
            },
            "CT": {
                "fillKey": "low",
                "enforcementReports": 32
            },
            "AK": {
                "fillKey": "highest",
                "enforcementReports": 32
            },
            "AR": {
                "fillKey": "highest",
                "enforcementReports": 32
            },
            "AL": {
                "fillKey": "highest",
                "enforcementReports": 32
            }
        }
    }
});


// Start it up
angular.element(document).ready(function () {
    angular.bootstrap(document, ['app']);

});
