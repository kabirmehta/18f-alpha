﻿### The Live Link ###
http://synergetics18falpha.azurewebsites.net/

### The Problem ###
Dr. Steven Chen, an attending ER physician, had a problem. He knew that there was valuable data regarding the geographic breakdown of enforcement reports in the openFDA database, but he found himself unable to access it in a useful way. He turned to the Synergetics team, led by Joshua Sebor, for help.

### The Team ###
Josh, himself an experienced Project Manager and Technical Architect, quickly assembled the team of Michael Dragilev, Back-end Web Developer, and Kabir Mehta, Front-end Web Developer. Josh let the team know that he would be fully responsible for the project, for managing the team and for ensuring the quality of the product.

### The User Research ###
The team began by having a discussion with Dr. Chen on a number of different data visualization options. After considering standard line and graph charts, they decided that a US-map based visualization would be most effective. The team also asked Dr. Chen to validate the design decisions with some of his colleagues, who agreed that this could be a helpful tool.

### The New Microsoft ###
The team began by selecting [ASP.NET 5.0](https://github.com/aspnet/home) as the technology platform. Reasons for this selection included the recent open-sourcing of this framework by Microsoft and the team’s extensive experience with the platform. Microsoft technologies are very common within government IT. MEAN or LAMP stacks may be more commonly seen as open-source technology stacks, yet having a Microsoft team will provide great value for the 18F initiative. Many government agencies rely heavily on .NET-backed systems and are eager to shift to a more rapid release cycle, agile style.

### The Open Source ###
In addition to the open-source ASP.NET platform, the team used [RestSharp](http://github.com/restsharp/RestSharp) to assist with API communication, [Angular.JS](http://github.com/angular/angular.js), [Bootstrap](http://github.com/twbs/bootstrap) for UI design, [DataMaps](http://github.com/markmarkoh/datamaps) for data visualization support, and Cruise Control.NET for continuous integration.

### The Containment, Continuous Integration, Monitoring, and Unit Tests ###
The project is deployed on the Microsoft Azure PaaS using the web site container approach, which hides the underlying infrastructure completely, requiring only a deployed web project. This project was written by the team with the Test-Driven Development methodology, requiring unit tests to be written as part of initial development rather than after the fact. The team encountered problems while running the unit tests, because the Visual Studio 2015 Release Candidate was not capable of running common unit test applications properly. Due to this, the solution contains a custom-written test framework. Deployment, continuous integration, and configuration management were all handled by the team’s Cruise Control.NET software. The team also configured Azure’s native monitoring capabilities to enable continuous monitoring of the production site and to allow proactive alerts to be sent to the team prior to the occurrence of any issues that might impact users.

### The Agile Approach ###
The team used an agile, iterative process to address potential improvements and fixes to the project. The initial proof-of-concept was shown to Dr. Chen for his feedback, then the team made modifications to the user interface based on his comments. The team also conducted a user feedback meeting once the prototype was complete. Since the prototype was intended as a Minimum Viable Product, the team’s intent was to release the prototype to the public as-is, and the user feedback meeting was held to collect a set of requirements for the second iteration of the project, intended to occur at the same time as initial public feedback is being gathered. The team also included Google Analytics within the project to collect quantitative data on the user experience and to track how users are navigating the site. This data will enable the team to make strategic decisions on future features and enhancements using FDA’s open data and related datasets. 

Lastly, the project was created with all open-source, freely licensed, free-of-charge platforms, and has been containerized and made available on a public repository. Any potential user should be able to deploy the project to any of the major IaaS or PaaS providers using the provided documentation.

### The Recap ###
 * Free, Open Source (ASP.NET 5.0, RestSharp, AngularJS, DataMaps, D3, TopoJson), and Permissive Licenses on All Used Frameworks and Libraries
 * Agile, Iterative, and U.S. [Digital Services Playbook](http://playbook.cio.gov/) Guided
 * Mobile, Tablet, and Desktop Friendly
 * User Research Conducted with Dr. Steven Chen, Attending Physician at [EmCare](https://www.emcare.com/)
 * Test Driven, Data Driven, Dependency Injected, Service Oriented, Asyncronous Architecture
 * Continuously Integrated, Containerized, Configuration Managed, Health Monitored, Activity Analyzed, and Unit Tested